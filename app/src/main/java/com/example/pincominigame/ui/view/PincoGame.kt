package com.example.pincominigame.ui.view

import android.content.Context
import android.graphics.*
import android.view.View
import androidx.compose.ui.graphics.toArgb
import com.example.pincominigame.data.Ball
import com.example.pincominigame.data.Coordinate
import com.example.pincominigame.data.static.CoefficientToColor
import com.example.pincominigame.ui.theme.Blue_dark
import com.example.pincominigame.ui.theme.Grey_dark
import com.example.pincominigame.ui.theme.Red
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*
import kotlin.random.Random

class PincoGame(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var bottomOffset = 0.0f
    private var pointRadius = 5.0f
    private var ballSpeed = 1.0f


    private var lines = 8
    private var ballBitmap = Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private var bottomLine = emptyList<Pair<String, Int>>()
    private val coefToColor = ArrayList<CoefficientToColor>() // bottomLineNumber, timer

    private val balls = ArrayList<Ball>()
    private var ballsToPut = 0

    private var mInterface: PincoInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            bottomOffset = mHeight * 0.05f
            createBall()
            createBottomLine()
            ballSpeed = mHeight / 60.0f
        }
    }

    override fun onDraw(canvas: Canvas) {
        addNewBalls()
        updateBalls()
        drawField(canvas)
        drawBalls(canvas)
    }

    //// Public

    fun setInterface(i: PincoInterface) {
        mInterface = i
    }

    fun setLinesAmount(amount: Int) {
        if(balls.isEmpty()) {
            lines = amount
            createBall()
            createBottomLine()
        }
    }

    fun start() {
        ballsToPut++
    }

    //// Private

    private fun drawField(c: Canvas) {
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.textSize = bottomOffset / 2
        p.color  = Grey_dark.toArgb()

        val spaceHorizontal = (mWidth / (lines + 1.0f)).toInt()
        val spaceVertical = ((mHeight - bottomOffset) / (lines + 1.0f)).toInt()

        val ballRadius = (mWidth / (lines + 2.0f) - pointRadius * 2) * 0.4f
        c.drawCircle(mWidth / 2.0f, spaceVertical / 2.0f, ballRadius, p)
        p.color  = Color.BLACK
        c.drawCircle(mWidth / 2.0f, spaceVertical / 2.0f, ballRadius * 0.8f, p)

        p.color  = Color.WHITE
        var points = 3
        for(y in 0 until lines) {
            val lineStart = mWidth / 2.0f - ((points - 1) / 2.0f * spaceHorizontal)
            for(n in 0 until points) {
                c.drawCircle(lineStart + n * spaceHorizontal, spaceVertical + y * spaceVertical.toFloat(), pointRadius, p)
            }
            points++
        }

        val coeffW = (mWidth - pointRadius * 2) / bottomLine.size.toFloat()
        for(coefficient in bottomLine.withIndex()) {
            val toColor = coefToColor.find { it.n == coefficient.index }
            var bottom = 0
            if(toColor != null) {
                toColor.timer-=2
                bottom =  -(toColor.timer / 2) % 30
                if(toColor.timer <= 0) coefToColor.remove(toColor)
            }

            p.color = coefficient.value.second
            c.drawRect(pointRadius  + coeffW * coefficient.index + coeffW * 0.05f,
                mHeight - bottomOffset * 0.8f + bottom,
                pointRadius  + coeffW * (coefficient.index + 1) - coeffW * 0.05f,
                mHeight.toFloat() + bottom, p)
            p.color = Blue_dark.toArgb()
            c.drawText(coefficient.value.first, pointRadius + coeffW * coefficient.index + coeffW * 0.5f, mHeight - bottomOffset * 0.2f + bottom, p)
        }
    }

    private fun drawBalls(c: Canvas) {
        for(ball in balls) c.drawBitmap(ballBitmap, ball.coordinate.x - ballBitmap.width / 2.0f,ball.coordinate.y - ballBitmap.width / 2.0f, Paint())
    }

    private fun createBall() {
        val ballRadius = (mWidth / (lines + 2.0f) - pointRadius * 2) * 0.4f

        val p = Paint()
        val gradient = LinearGradient(0.0f, 0.0f, ballRadius * 2, ballRadius * 2, Color.WHITE, Red.toArgb(), Shader.TileMode.MIRROR)
        p.isDither = true
        p.shader = gradient

        ballBitmap = Bitmap.createBitmap((ballRadius * 2).toInt(), (ballRadius * 2).toInt(), Bitmap.Config.ARGB_8888)
        val c = Canvas(ballBitmap)
        c.drawCircle(ballRadius, ballRadius, ballRadius, p)
        p.shader = null
        p.color = Red.toArgb()
        p.style = Paint.Style.STROKE
        p.strokeWidth = 2.0f
        c.drawCircle(ballRadius, ballRadius, ballRadius, p)
    }

    private fun addNewBalls() {
        val initY = mHeight / (lines + 1.0f) * 0.5f
        if(ballsToPut > 0) {
            if(balls.none { (it.coordinate.x - mWidth / 2.0f).absoluteValue <= ballBitmap.width
                        && (it.coordinate.y - initY).absoluteValue <= ballBitmap.height }) {
                putBall()
                ballsToPut--
            }
        }
    }

    private fun putBall() {
        val space = mHeight / (lines + 1.0f)
        balls.add(Ball(Coordinate(mWidth  / 2.0f, space / 2.0f)))
    }

    private fun createBottomLine() {
        val line = ArrayList<Pair<String, Int>>()

        val coeffStep = PI * 0.4f / ((lines + 1) / 2)
        for(n in 0 until ((lines + 1) / 2)) {
            val coefficient = 1 + tan(n * coeffStep)
            val color = Color.rgb(70 + n * (124 / ((lines + 1) / 2)), 194 - n * ( 26 / ((lines + 1) / 2)), 37)
            val text = if(coefficient.rem(1) < 0.1f) coefficient.toInt().toString()
            else String.format("%.1f", coefficient)
            line.add(text to color)
        }
        val startLine = line.reversed()

        val odd = lines % 2 != 0
        val green = Color.rgb(70, 194, 37)
        val bLine = ArrayList(startLine)
        if(odd) {
            bLine.add("0.7" to green)
            bLine.add("0.7" to green)
        } else bLine.add("0.5" to green)
        bLine.addAll(line)
        bottomLine = bLine
    }

    private fun updateBalls() {
        val spaceHorizontal = (mWidth / (lines + 1.0f)).toInt()
        val spaceVertical = ((mHeight - bottomOffset) / (lines + 1.0f)).toInt()
        val ballRadius = (mWidth / (lines + 2.0f) - pointRadius * 2) * 0.4f
        var p = 0
        while(p < balls.size) {
            val ball = balls[p]
            var jump = false
            var points = 3
            for(y in 0 until lines) {
                val lineStart = mWidth / 2.0f - ((points - 1) / 2.0f * spaceHorizontal)
                for(n in 0 until points) {
                    if((ball.coordinate.x - (lineStart + n * spaceHorizontal)).absoluteValue <= ballRadius + pointRadius &&
                            (ball.coordinate.y - (spaceVertical + y * spaceVertical.toFloat())).absoluteValue <= ballRadius + pointRadius &&
                            ball.target.x == 0.0f) {
                        ball.target.y = ball.coordinate.y
                        ball.target.x = ball.coordinate.x + ((spaceHorizontal * 0.5f) *  if (Random.nextInt(2) == 0) 1 else -1)
                        jump = true
                    }
                }
                points++
            }
            if(!jump) {
                if(ball.target.x != 0.0f) {
                    val dX = spaceHorizontal / 30.0f * ( ball.target.x - ball.coordinate.x).sign
                    val dY = cos(PI * ( (ball.target.x.absoluteValue - ball.coordinate.x.absoluteValue).absoluteValue) / (spaceHorizontal / 2.0f)) * spaceVertical / 30
                    ball.coordinate.x += dX
                    ball.coordinate.y += dY.toFloat()
                    if((ball.target.x.absoluteValue - ball.coordinate.x.absoluteValue).absoluteValue <= spaceHorizontal / 30.0f) {
                        ball.coordinate.x = ball.target.x
                        ball.coordinate.y = ball.target.y
                        ball.target = Coordinate(0.0f, 0.0f)
                    }
                } else ball.coordinate.y += ballSpeed
            }
            if(ball.coordinate.y >= mHeight - bottomOffset * 0.5f - ballRadius) {
                val coeffN = ball.coordinate.x.toInt() / ((mWidth - pointRadius * 2) / bottomLine.size).toInt()
                val targetCoeff = if(coeffN < bottomLine.size) bottomLine[coeffN].first.toFloatOrNull()
                else bottomLine.last().first.toFloatOrNull()
                mInterface?.finish(targetCoeff ?: 15.0f)
                coefToColor.add(CoefficientToColor(coeffN , 30))

                balls.removeAt(p)
                p--
            }
            p++
        }
    }

    interface PincoInterface {
        fun finish(coefficient: Float)
    }
}