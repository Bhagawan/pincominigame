package com.example.pincominigame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Blue_light_2 = Color(0xFF5596C2)
val Blue_light = Color(0xFF3C678D)
val Blue_dark = Color(0xFF192D4B)
val Grey_dark = Color(0xFF202020)
val Red = Color(0xFFC22525)
val Yellow_light = Color(0xFFDFC99D)
val Yellow = Color(0xFFE7AA2F)
val Yellow_dark = Color(0xFFD56610)
val Purple_light = Color(0xFF7A61C0)
val Purple_light_2 = Color(0xFFA190CF)
val Purple_grey = Color(0xFF4C3E75)