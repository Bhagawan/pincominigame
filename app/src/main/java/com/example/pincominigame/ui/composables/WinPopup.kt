package com.example.pincominigame.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.background
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pincominigame.R
import com.example.pincominigame.ui.theme.Yellow_dark

@Composable
fun WinPopup(amount: Float?, visible: Boolean = false, modifier: Modifier = Modifier) {
    AnimatedVisibility(
        visible = visible,
        enter = scaleIn(),
        exit = scaleOut()
    ) {
        Text(amount?.let{stringResource(id = R.string.win_string, String.format("%.2f", amount))}?:"", fontSize = 15.sp, textAlign = TextAlign.Center, color = Color.White,
            modifier = modifier.background(Yellow_dark, RoundedCornerShape(5.dp)), lineHeight = 16.sp)
    }
}