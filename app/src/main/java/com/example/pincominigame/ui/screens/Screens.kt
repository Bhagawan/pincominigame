package com.example.pincominigame.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    GAME_SCREEN("game")
}