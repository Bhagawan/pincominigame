package com.example.pincominigame.ui.screens

import android.webkit.WebView
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView

@Composable
fun WebViewScreen(webView: WebView, homeUrl: String) {
    var init  by rememberSaveable { mutableStateOf(true) }
    AndroidView(factory = { webView },
        modifier = Modifier.fillMaxSize(),
        update = {
            if (init) {
                it.loadUrl(homeUrl)
                init = false
            }
        })
}