package com.example.pincominigame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import coil.compose.rememberImagePainter
import com.example.pincominigame.data.static.UrlLogo
import com.example.pincominigame.ui.theme.Blue_dark
import com.example.pincominigame.ui.theme.Blue_light

@Composable
fun SplashScreen() {
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val brush = Brush.linearGradient(
            0.0f to Blue_light,
            1.0f to Blue_dark,
            start = Offset.Zero,
            end = Offset(constraints.maxWidth.toFloat(), constraints.maxHeight.toFloat())
        )
        Column(
            modifier = Modifier.fillMaxSize().background(brush),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Image(rememberImagePainter(UrlLogo), contentDescription = null)
        }
    }
}