package com.example.pincominigame.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pincominigame.ui.theme.Purple_light
import com.example.pincominigame.ui.theme.Purple_light_2

@Composable
fun TextWithIcon(text: String, icon: Painter, iconBackgroundColor: Color = Purple_light, textSize: Int = 10, textColor: Color = Purple_light_2, modifier: Modifier = Modifier) {
    Row(modifier = modifier.height(22.dp).padding(vertical = 2.dp, horizontal = 5.dp), verticalAlignment = Alignment.CenterVertically) {
        Image(icon, contentDescription = null, contentScale = ContentScale.Fit,
            modifier = Modifier
                .background(iconBackgroundColor, RoundedCornerShape(5.dp))
                .padding(5.dp)
                .shadow(1.dp, RoundedCornerShape(5.dp), clip = true))
        Text(text, fontSize = textSize.sp, color = textColor, textAlign = TextAlign.Center, modifier = Modifier.weight(1.0f, true))
    }
}