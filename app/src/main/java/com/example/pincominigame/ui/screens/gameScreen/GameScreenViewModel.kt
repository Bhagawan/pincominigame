package com.example.pincominigame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pincominigame.data.static.CurrentAppData
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameScreenViewModel:ViewModel() {
    private val _startCommand = MutableSharedFlow<Boolean>()
    val startCommand = _startCommand.asSharedFlow()

    private val _selectLines = MutableSharedFlow<Int>(1)
    val selectLines = _selectLines.asSharedFlow()

    private val _winPopup = MutableStateFlow<Float?>(null)
    val winPopup = _winPopup.asStateFlow()

    private val _betAmount = MutableStateFlow(CurrentAppData.minBet)
    val betAmount = _betAmount.asStateFlow()

    private val _balanceAmount = MutableStateFlow(CurrentAppData.balance)
    val balanceAmount = _balanceAmount.asStateFlow()

    fun start() {
        CurrentAppData.balance = (CurrentAppData.balance - betAmount.value).coerceAtLeast(0.0f)
        if (CurrentAppData.balance == 0.0f) CurrentAppData.balance = 1000.0f
        _balanceAmount.tryEmit(CurrentAppData.balance)
        viewModelScope.launch {
            _startCommand.emit(true)
        }
    }

    fun selectLinesAmount(amount: Int) {
        _selectLines.tryEmit(amount)
    }

    fun win(coefficient: Float) {
        CurrentAppData.balance = CurrentAppData.balance + betAmount.value * coefficient
        _balanceAmount.tryEmit(CurrentAppData.balance)
        _winPopup.tryEmit(coefficient)
        viewModelScope.launch {
            delay(3000)
            _winPopup.tryEmit(null)
        }
    }

    fun setBet(amount: Int) {
        _betAmount.tryEmit(amount)
    }

    fun increaseBet(amount: Int) {
        setBet(betAmount.value + amount)
    }
}