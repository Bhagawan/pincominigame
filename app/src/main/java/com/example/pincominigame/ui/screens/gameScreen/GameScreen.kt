package com.example.pincominigame.ui.screens.gameScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.pincominigame.R
import com.example.pincominigame.data.static.CurrentAppData
import com.example.pincominigame.ui.composables.TextWithIcon
import com.example.pincominigame.ui.composables.WinPopup
import com.example.pincominigame.ui.theme.*
import com.example.pincominigame.ui.view.PincoGame
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun GameScreen() {
    val viewModel = viewModel(GameScreenViewModel::class.java)
    val buttonInteractionSource = remember { MutableInteractionSource() }
    val buttonIsPressed by buttonInteractionSource.collectIsPressedAsState()
    var init = remember { true }

    val winPopup by viewModel.winPopup.collectAsState()

    val betAmount by viewModel.betAmount.collectAsState()
    val balance by viewModel.balanceAmount.collectAsState()

    val horizontalBrush = Brush.linearGradient(
        0.0f to Blue_dark,
        1.0f to Purple40
    )

    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val brush = Brush.linearGradient(
            0.0f to Blue_light,
            0.7f to Blue_dark,
            1.0f to Purple40,
            start = Offset.Zero,
            end = Offset(constraints.maxWidth.toFloat(), constraints.maxHeight.toFloat())
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(brush)
                .padding(10.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Box(modifier = Modifier
                .fillMaxWidth()
                .weight(1.0f, true)) {
                AndroidView(factory = { PincoGame(it) },
                    modifier = Modifier
                        .fillMaxWidth(0.9f)
                        .fillMaxHeight()
                        .align(Alignment.Center),
                    update = { game ->
                        if(init) {
                            viewModel.startCommand.onEach {
                                if(it) { game.start() }
                            }.launchIn(viewModel.viewModelScope)

                            viewModel.selectLines.onEach {
                                game.setLinesAmount(it)
                            }.launchIn(viewModel.viewModelScope)

                            game.setInterface(object: PincoGame.PincoInterface {
                                override fun finish(coefficient: Float) {
                                    viewModel.win(coefficient)
                                }
                            })
                            init = false
                        }
                    })
                Column(modifier = Modifier
                    .fillMaxWidth(0.09f)
                    .align(Alignment.TopEnd), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.spacedBy(2.dp)) {
                    Text(stringResource(id = R.string.lines), fontSize = 10.sp, textAlign = TextAlign.Center, color = Blue_light_2, modifier = Modifier.fillMaxWidth())
                    for(n in 8..16) {
                        Box(modifier = Modifier
                            .fillMaxWidth()
                            .background(Blue_light, RoundedCornerShape(5.dp))
                            .clickable(
                                MutableInteractionSource(),
                                null
                            ) { viewModel.selectLinesAmount(n) }, contentAlignment = Alignment.Center) {
                            Text(n.toString(), fontSize = 9.sp, textAlign = TextAlign.Center, color = Blue_light_2, lineHeight = 15.sp)
                        }
                    }
                }
            }

            Box(modifier = Modifier
                .fillMaxWidth()
                .height(40.dp)
                .padding(10.dp), contentAlignment = Alignment.Center) {
                WinPopup(amount = winPopup, visible = winPopup != null, modifier = Modifier.fillMaxSize(0.9f))
            }

            Row(modifier = Modifier.fillMaxWidth(0.98f), verticalAlignment = Alignment.Bottom) {
                Column(modifier = Modifier.weight(1.0f, true), horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(stringResource(id = R.string.risk_level), fontSize = 15.sp, textAlign = TextAlign.Center, color = Purple_light, modifier = Modifier.fillMaxWidth())
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .background(
                            Purple_light,
                            RoundedCornerShape(topStart = 10.dp, topEnd = 10.dp)
                        )
                        .padding(3.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                        TextWithIcon(text = stringResource(id = R.string.risk_high), painterResource(id = R.drawable.ic_fire_simple), modifier = Modifier.fillMaxWidth())
                        HorizontalDivider(modifier = Modifier.fillMaxWidth(0.8f), thickness = 1.dp, color = Purple_light_2)
                        TextWithIcon(text = stringResource(id = R.string.risk_normal), painterResource(id = R.drawable.ic_tea_cup), modifier = Modifier.fillMaxWidth())
                        HorizontalDivider(modifier = Modifier.fillMaxWidth(0.8f), thickness = 1.dp, color = Purple_light_2)
                        TextWithIcon(text = stringResource(id = R.string.risk_low), painterResource(id = R.drawable.ic_ice_cream), modifier = Modifier.fillMaxWidth())
                    }
                }

                Box(modifier = Modifier.weight(1.5f, true), contentAlignment = Alignment.Center) {
                    TextButton(onClick = { viewModel.start() },
                        interactionSource = buttonInteractionSource, modifier = Modifier
                            .clipToBounds()
                            .padding(vertical = 10.dp)
                            .size(100.dp)
                            .border(
                                width = 5.dp,
                                color = if (buttonIsPressed) Yellow_dark else Yellow,
                                shape = CircleShape
                            )
                            .background(Yellow_light, CircleShape)) {
                        Text(
                            stringResource(id = R.string.btn_start),
                            fontSize = 30.0f.sp,
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Center,
                            color = Yellow
                        )
                    }
                }

                Column(modifier = Modifier.weight(1.0f, true), horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(stringResource(id = R.string.bet_mode), fontSize = 15.sp, textAlign = TextAlign.Center, color = Purple_light, modifier = Modifier.fillMaxWidth())
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .background(
                            Purple_light,
                            RoundedCornerShape(topStart = 10.dp, topEnd = 10.dp)
                        )
                        .padding(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                        TextWithIcon(text = stringResource(id = R.string.mode_manual), painterResource(id = R.drawable.ic_letter_m), modifier = Modifier.fillMaxWidth())
                        HorizontalDivider(modifier = Modifier.fillMaxWidth(0.8f), thickness = 1.dp, color = Purple_light_2)
                        TextWithIcon(text = stringResource(id = R.string.mode_auto), painterResource(id = R.drawable.ic_letter_a), modifier = Modifier.fillMaxWidth())
                    }
                }
            }

            Row(modifier = Modifier
                .fillMaxWidth(0.98f)
                .background(horizontalBrush)
                .padding(vertical = 5.dp), verticalAlignment = Alignment.CenterVertically) {
                Text(stringResource(id = R.string.min),
                    fontSize = 13.sp,
                    textAlign = TextAlign.Center,
                    color = if(betAmount == CurrentAppData.minBet) Purple_light else Color.White,
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(end = 1.dp)
                        .background(
                            color = if (betAmount == CurrentAppData.minBet) Purple_grey else Purple_light,
                            shape = RoundedCornerShape(5.dp)
                        )
                        .clickable { viewModel.setBet(1) })
                Text("-",
                    fontSize = 13.sp,
                    textAlign = TextAlign.Center,
                    color = if(betAmount == CurrentAppData.minBet) Purple_light else Color.White,
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(start = 1.dp)
                        .background(
                            color = if (betAmount == CurrentAppData.minBet) Purple_grey else Purple_light,
                            shape = RoundedCornerShape(5.dp)
                        )
                        .clickable { viewModel.increaseBet(-1) })

                Text(
                    stringResource(id = R.string.bet_string, String.format("%.2f", betAmount.toFloat())),
                    fontSize = 17.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    color = Color.White,
                    modifier = Modifier.weight(1.5f))

                Text("+",
                    fontSize = 13.sp,
                    textAlign = TextAlign.Center,
                    color = if(betAmount == balance.toInt()) Purple_light else Color.White,
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(end = 1.dp)
                        .background(
                            color = if (betAmount == balance.toInt()) Purple_grey else Purple_light,
                            shape = RoundedCornerShape(5.dp)
                        )
                        .clickable { viewModel.increaseBet(1) })
                Text(
                    stringResource(id = R.string.max),
                    fontSize = 13.sp,
                    textAlign = TextAlign.Center,
                    color = if(betAmount == balance.toInt()) Purple_light else Color.White,
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(start = 1.dp)
                        .background(
                            color = if (betAmount == balance.toInt()) Purple_grey else Purple_light,
                            shape = RoundedCornerShape(5.dp)
                        )
                        .clickable { viewModel.setBet(balance.toInt()) })
            }

            Text(
                stringResource(id = R.string.balance_string, String.format("%.2f", balance)),
                fontSize = 17.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.fillMaxWidth(0.95f))
        }
    }
}