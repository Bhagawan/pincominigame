package com.example.pincominigame.util

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class SavedPrefs {
    companion object {
        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("Quantum", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }

//        fun addInvestment(context: Context, investment: Investment) {
//            val shP = context.getSharedPreferences("Quantum", Context.MODE_PRIVATE)
//
//            val g = Gson()
//            val list = g.fromJson<List<Investment>>(shP.getString("investments", "[]") ?: "[]")
//            val newList = list.plus(investment).sortedBy { it.date }
//
//            shP.edit().putString("investments", g.toJson(newList)).apply()
//        }
//
//        fun getInvestmentsList(context: Context): List<Investment> = Gson()
//            .fromJson<List<Investment>>(context.getSharedPreferences("Quantum", Context.MODE_PRIVATE).getString("investments", "[]") ?: "[]")

        fun saveNotificationSettings(context: Context, on: Boolean) = context
            .getSharedPreferences("Quantum", Context.MODE_PRIVATE)
            .edit()
            .putBoolean("notification", on)
            .apply()

        fun getNotificationSettings(context: Context): Boolean =
            context.getSharedPreferences("Quantum", Context.MODE_PRIVATE).getBoolean("notification", false)

        fun saveFontSize(context: Context, size: Float) = context
            .getSharedPreferences("Quantum", Context.MODE_PRIVATE)
            .edit()
            .putFloat("font_size", size)
            .apply()

        fun getFontSize(context: Context): Float =
            context.getSharedPreferences("Quantum", Context.MODE_PRIVATE).getFloat("font_size", 20.0f)

        private inline fun <reified T> Gson.fromJson(json: String): T = fromJson(json, object: TypeToken<T>() {}.type)
    }
}