package com.example.pincominigame.data.static

data class CoefficientToColor(val n: Int, var timer: Int)
