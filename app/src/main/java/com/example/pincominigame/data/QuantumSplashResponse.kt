package com.example.pincominigame.data

import androidx.annotation.Keep

@Keep
data class QuantumSplashResponse(val url : String)