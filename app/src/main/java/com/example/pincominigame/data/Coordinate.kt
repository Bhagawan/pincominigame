package com.example.pincominigame.data

data class Coordinate(var x: Float, var y: Float)
