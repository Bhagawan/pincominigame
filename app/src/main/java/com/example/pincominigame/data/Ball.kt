package com.example.pincominigame.data

data class Ball(var coordinate: Coordinate, var target: Coordinate = Coordinate(0.0f, 0.0f))
